### Key items (attacking):
 -  s

### Attacks/transitions from position
- [Long step](/attacks/Long%20step.md)
- [Back step](/attacks/Back%20step.md)
- [Leg drag](/attacks/Lag%20drag.md)
- [Toreando](../attacks/Toreando.md)
- [Pressure pass](../attacks/Preasure_pass.md)

### Image
TBD

### Links
TBD

#### Type 
#position 