### Key items (attacking):
- 

### Attacks/transitions from position
 - [Knee slide](../attacks/Knee%20slide.md)
 - [Headquoters](../positions/Headquarters.md)
 - [Knee slide](../attacks/Knee%20slide.md) -> [backstep](../attacks/Back%20step.md)
 - [Knee slide](../attacks/Knee%20slide.md) > [crazy dog](../attacks/Crazy%20dog.md) > [side controle](../positions/Side%20control.md)
 - unbalance forward -> kick the leg -> stand up with one leg



### Links
TBD

#### Type 
#position

![delariva.png](../images/delariva.png)
