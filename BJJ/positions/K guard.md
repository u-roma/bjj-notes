### Key items (attacking):
-  s

### Attacks/transitions from position
- matrix (imbalance forward)
- leg lock (on the bell) (imbalance on a back)

### Image
![img.png](../images/k%20guard.png)

### Links
https://www.youtube.com/watch?v=jJpPlNf2s5Y

#### Type
#position 