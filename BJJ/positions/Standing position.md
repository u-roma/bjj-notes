### Key items (attacking):
 - 

### Attacks/transitions from position
- [Pull guard](../attacks/Pull%20guard.md)
- [Collar drag](../attacks/Collar%20drag.md)

### Image
![../images/Pasted%20image%2020230719183328.png](../images/Pasted%20image%2020230719183328.png)
### Links
TBD

#### Type 
#position 