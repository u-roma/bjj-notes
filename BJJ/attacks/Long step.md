

### Attacks/transitions from to
FROM [[Knee shield]]
TO

### Key items
- you should not fall fully on your back, better to keep shoulders on your albow
- as soon as you throw your self and your body touches mat - bounce on your foots
- clear opponents foot (so he can not do knee shield)
- put constant preasure on the opponent chest/shoulder by your forehead

### Image 
![img.png](../images/long%20step.png)

### Links
https://www.youtube.com/watch?v=zdLtxVHygMQ

#### Type
#attacks

submission
lock
choke