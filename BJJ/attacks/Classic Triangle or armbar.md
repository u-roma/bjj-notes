### Key items :
- pull hard/long the hand that you control. Keep opponent overextended
- keep controlling caller - otherwise open will slip out of triangle
- put your leg in his thigh
- Extend him to great extend


### Attacks/transitions from to
FROM [closed guard](../positions/Closed%20guard.md)
TO

### Image
![img_2.png](../images/ClassicTriangle1.png)
![img.png](../images/ClassicTriangle2.png)
![img_1.png](../images/ClassicTriangle3.png)

### Links
https://youtu.be/8-8-YfTsajE?si=jhUur3RStYPIpD4X

#### Type
#attacks
