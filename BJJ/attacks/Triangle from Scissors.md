### Key items :
- pull hard the hand that you control
- keep controlling caller - otherwise open will slip out of triangle
- wait until person goes under your leg


### Attacks/transitions from to
FROM [closed guard](../positions/Closed%20guard.md)
TO

### Image
![img.png](../images/TriangleFromPushPull.png)
![img_1.png](../images/TriangleFromPushPull2.png)

### Links
https://youtu.be/FJ1yMz0u90M?si=73qm4JYu173cYRKs&t=60

#### Type
#attacks
