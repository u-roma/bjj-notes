
### Key items :
- should be done only if opponent weight is far from you (on his legs)
- you should open your legs (ope closed guard)
- you should catch opponent by elbow

### Attacks/transitions from to
FROM [closed guard](../positions/Closed%20guard.md)
TO [mount](../positions/Mount.md)

### Image
![img.png](../images/hip_bump.png)

### Links
https://youtu.be/Vx57_Bpd1rA?si=DnvCilJd-pNvH6AL&t=103

#### Type
#attacks


