### Key items :
- control wrist and bycept
- close his elbow by your chest


### Attacks/transitions from to
FROM [closed guard](../positions/Closed%20guard.md)
TO

### Image
![img.png](../images/backAttackFromTriangle.png)
![img.png](../images/backAttackFromTriangle2.png)
![img.png](../images/backAttackFromTriangle3.png)

### Links
https://youtu.be/MubbYrvhahY?si=v-YAEMJLkbb7vOYy&t=386

#### Type
#attacks
