
### Key items :
- sleeve + the same collar grip
- tou should stay on your elbow and stretch the opponent by the sleeve
- your leg should go up high to the opponent`s armpit 

### Attacks/transitions from to
FROM [closed guard](../positions/Closed%20guard.md)
TO [mount](../positions/Mount.md)

### Image
![img.png](../images/scissor_sweep.png)

### Links
https://www.youtube.com/watch?v=Vx57_Bpd1rA

#### Type
#attacks


